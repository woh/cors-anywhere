FROM keymetrics/pm2:10-alpine

RUN mkdir -p /app
WORKDIR /app

COPY . .

RUN npm ci --only=production

EXPOSE 8080

CMD ["pm2-runtime", "start", "ecosystem.config.js"]

